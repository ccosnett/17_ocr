#!/home/ubuntu/anaconda3/envs/py373/bin/python

#paths vars


# getting last 5 characters of name of containing directory
import os
striing = os.getcwd()
last5 = striing[-5:]

print('last5 = ',last5)

ocr_module_output = './ocr_tesseract_output_' + last5 + '.csv'
this_weeks_RM = './RM_BUY_' + last5 + '_2021.csv'





print('hi')

########## imports ##########

import regex
import numpy as nb
import pytesseract
import pandas as pd
import numpy as np
import math
import re
import time
import regex
from pandarallel import pandarallel
try:
    from PIL import Image
except ImportError:
    import Image
#pandarallel.initialize(nb_workers=14, progress_bar=True)
pandarallel.initialize(nb_workers=14)
from tqdm.auto import tqdm
tqdm.pandas(desc="attach column")

start_time = time.time()

# here we just setup a dataframe called 'out'. The rest of this program attaches columns to this dataframe.

# create url from id
def url(row):
    return "https://www.rightmove.co.uk/properties/"+str(row['id'])



def attach(func):
    '''this function takes as its argument a function which it uses to create and attach a column to the right of the dataframe called '''
    out[func] = out.parallel_apply(    eval(func),    axis=1)
    #out[func] = out.progress_apply(    eval(func),    axis=1)

    
    
    
#HERE we make a small data frame from this weeks rightmove
print('loading description and summary box data')
inputDF = pd.read_csv(this_weeks_RM)[["id","description","sqft","sqm"]]
output = pd.DataFrame()
output["id"] = inputDF["id"]
output["description"] = inputDF["description"]
output["summary_box_m2"] = inputDF["sqm"]
output["summary_box_f2"] = inputDF["sqft"]
id_description_summary_box = output
print('data loaded')    
    
print('loading ocr output data')
input_df=pd.read_csv(ocr_module_output)


out = pd.DataFrame()
out["id"] = input_df["id"]
print(len(out))
attach('url')
out["img_file_1"] = input_df["img_file_1"]
out["img_file_2"] = input_df["img_file_2"]
out["img_file_3"] = input_df["img_file_3"]
out["img_file_4"] = input_df["img_file_4"]
out["img_file_5"] = input_df["img_file_5"]
out["tesseract_ocr_file_1"] = input_df["tesseract_ocr_file_1"]
out["tesseract_ocr_file_2"] = input_df["tesseract_ocr_file_2"]
out["tesseract_ocr_file_3"] = input_df["tesseract_ocr_file_3"]
out["tesseract_ocr_file_4"] = input_df["tesseract_ocr_file_4"]
out["tesseract_ocr_file_5"] = input_df["tesseract_ocr_file_5"]
out["ocr_concatenation"] = input_df["ocr_concatenation"]
#out = out[0:1000]
print('length = ', len(out))
#out["category"] = input_df["category"]
#out["imgType"] = input_df["imgType"]
#out["order"] = input_df["order"]
#out["filename"] = input_df["filename"]
#out["ocr_concatenation"] = input_df["colfoo2"]

#def raw(row):
#    return r'{}'.format(row[""])


def search(regex, string):
    '''this function returns a boolean after searching a string for a regular expression'''
    return bool(re.search(regex, str(string).lower()))

################# Boolean functions ########################################

def single_true(iterable):
    '''single true takes a list of booleans called iterable as its argument and returns True if a single boolean is True'''
    i = iter(iterable)
    return any(i) and not any(i)

def isnum(s):
    '''checks if num'''
    ss = str(s)
    if ss.replace('.','',1).isdigit():
        return float(ss) + 0.000001
    else:
        return nb.nan

def div(ratio):
    return abs((ratio/10.7639) - 1.00000000000000000001)


def div_m2m2(ratio):
    return abs(ratio - 1.00000000000000000001)


def div_under_3_percent(divergence):
    return divergence < 0.03

############################## COLUMN FUNCTIONS #####################################
############################## ocr #####################################

def _001__ocr_tesseract(row):
    path = row["filename"]
  #  print(str(row["id"])+"\n")
    return pytesseract.image_to_string(Image.open(path))


############################## ocr #####################################

def _002__ocr_wolfram(row):
    path = row["filename"]
  #  print(str(row["id"])+"\n")
    return "."#pytesseract.image_to_string(Image.open(path))


############################## ocr #####################################

def _003__ocr_package_3(row):
    path = row["filename"]
  #  print(str(row["id"])+"\n")
    return "."#pytesseract.image_to_string(Image.open(path))



############################## DESCRIPTION FUNCTIONS ####################################
############################## DESCRIPTION FUNCTIONS ####################################
############################## DESCRIPTION FUNCTIONS ####################################
def test1(row):
    return row['id']


############################## _004__description #####################################
def _004__description(row):
    id_ = row["id"]
   #print( ))
    string = id_description_summary_box.loc[id_description_summary_box.id == id_, 'description']
    if len(string) > 0:
        return string.values[0]
    else:
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        return '.'
############################## _005__description_m2 #####################################

#desRules = {Nothing
#   , a : NumberString ~~ b : "sq.ft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " SQ FT" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sq m" :> {"description_m2" -> a}
#   , a : NumberString ~~ b : " sq. ft." :> {"description_f2" -> a}
#   , a : NumberString ~~ b : "sq.m" :> {"description_m2" -> a}
#   , a : NumberString ~~ b : " Sq/ft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sqft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sq.ft." :> {"description_f2" -> a}
#   , a : NumberString ~~ b : "sq ft" :> {"description_f2" -> a}
#   };

def _005__description_m2(row):
    string = row['_004__description'].lower()

    #string = row["ocr_concatenation"].lower()

    # 1    , a : NumberString ~~ b : " sq m" :> {"description_m2" -> a}
    patternm1 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssq\sm)"

    # 2    , a : NumberString ~~ b : "sq.m" :> {"description_m2" -> a}
    patternm2 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=sq\.m)"

    c1 = case(string, patternm1)
    c2 = case(string, patternm2)

    if c1 != nb.nan:
        return c1
    elif c2 != nb.nan:
        return c2
    else:
        return c2


############################## _006__description_f2 #####################################


#desRules = {Nothing
#   , a : NumberString ~~ b : "sq.ft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " SQ FT" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sq m" :> {"description_m2" -> a}
#   , a : NumberString ~~ b : " sq. ft." :> {"description_f2" -> a}
#   , a : NumberString ~~ b : "sq.m" :> {"description_m2" -> a}
#   , a : NumberString ~~ b : " Sq/ft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sqft" :> {"description_f2" -> a}
#   , a : NumberString ~~ b : " sq.ft." :> {"description_f2" -> a}
#   , a : NumberString ~~ b : "sq ft" :> {"description_f2" -> a}
#   };

def _006__description_f2(row):
    string = row['_004__description'].lower()
    #print("here")
    # 1   , a : NumberString ~~ b : "sq.ft" :> a
    patternf1 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=sq\.ft)"

    # 2   , a : NumberString ~~ b : " SQ FT" :> a         #CHANGED TO LOWERCASE
    patternf2 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssq\sft)"

    # 3   , a : NumberString ~~ b : " sq. ft." :> {"description_f2" -> a} #WITH OPTIONAL \. AT END
    patternf3 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssq\.\sft\.?)"

    # 4   , a : NumberString ~~ b : " Sq/ft" :> {"description_f2" -> a}
    patternf4 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssq/ft)"

    # 5   , a : NumberString ~~ b : " sqft" :> {"description_f2" -> a}
    patternf5 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssqft)"

    # 6   , a : NumberString ~~ b : " sq.ft." :> {"description_f2" -> a}
    patternf6 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=\ssq.ft.?)"

    # 7   , a : NumberString ~~ b : "sq ft" :> {"description_f2" -> a}
    patternf7 = r"(?<=\s)\d{1,6}\.?\d{0,4}(?=sq ft)"

    c1 = case(string, patternf1)
    c2 = case(string, patternf2)
    c3 = case(string, patternf3)
    c4 = case(string, patternf4)
    c5 = case(string, patternf5)
    c6 = case(string, patternf6)
    c7 = case(string, patternf7)

    if c1 != nb.nan:
        return c1
    elif c2 != nb.nan:
        return c2
    elif c3 != nb.nan:
        return c3
    elif c4 != nb.nan:
        return c4
    elif c5 != nb.nan:
        return c5
    elif c6 != nb.nan:
        return c6
    elif c7 != nb.nan:
        return c7
    else:
        return c7



############################## _007__description_f2_m2_ratio #############################

def _007__description_f2_m2_ratio(row):
    m2 = row['_005__description_m2']
    f2 = row['_006__description_f2']
    return isnum(f2) / isnum(m2)


############################## _008__description_f2_m2_ratio_divergence ################

def _008__description_f2_m2_ratio_divergence(row):
    ratio = row['_007__description_f2_m2_ratio']
    return div(ratio)


############################## _009__description_test_1 ################

def _009__description_test_1(row):
    divergence = row['_008__description_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _010__description_f2_m2_inverse_ratio #################


def _010__description_f2_m2_inverse_ratio(row):
    ratio = row['_007__description_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _011__description_f2_m2_inverse_ratio_divergence #################


def _011__description_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_010__description_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _012__description_test_2 #################


def _012__description_test_2(row):
    inverse_divergence = row['_011__description_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)



############################## SUMMARY BOX FUNCTIONS ####################################
############################## SUMMARY BOX FUNCTIONS ####################################
############################## SUMMARY BOX FUNCTIONS ####################################



############################## _014__summary_box_m2 #####################################


def _014__summary_box_m2(row):
    id_ = row["id"]
 #   print(row['id'])
    value = id_description_summary_box.loc[id_description_summary_box.id == id_, 'summary_box_m2']
    if len(value) > 0:
        return value.values[0]
    else:
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        return nb.nan

############################## _015__summary_box_f2 #####################################

def _015__summary_box_f2(row):
    id_ = row["id"]
    value = id_description_summary_box.loc[id_description_summary_box.id == id_, 'summary_box_f2']
    if len(value) > 0:
        return value.values[0]
    else:
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        print('NOT IN RM FILE\n')
        return nb.nan


############################## _016__summary_box_f2_m2_ratio #############################

def _016__summary_box_f2_m2_ratio(row):
    m2 = row['_014__summary_box_m2']
    f2 = row['_015__summary_box_f2']
    return isnum(f2) / isnum(m2)


############################## _017__summary_box_f2_m2_ratio_divergence ################

def _017__summary_box_f2_m2_ratio_divergence(row):
    ratio = row['_016__summary_box_f2_m2_ratio']
    return div(ratio)


############################## _018__summary_box_test_1 ################

def _018__summary_box_test_1(row):
    divergence = row['_017__summary_box_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _019__summary_box_f2_m2_inverse_ratio #################


def _019__summary_box_f2_m2_inverse_ratio(row):
    ratio = row['_016__summary_box_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _020__summary_box_f2_m2_inverse_ratio_divergence #################


def _020__summary_box_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_019__summary_box_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _021__summary_box_test_2 #################


def _021__summary_box_test_2(row):
    inverse_divergence = row['_020__summary_box_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)




############################## TOTAL FUNCTIONS #####################################
############################## TOTAL FUNCTIONS #####################################
############################## TOTAL FUNCTIONS #####################################
#1+0*8 = 1

############################## _2__total_m2 #####################################

def case(string, pattern):
    l=regex.findall(pattern, string.lower())
    if len(l) > 0:
        return float(l[0])
    else:
        return nb.nan

############################## _2__total_m2 #####################################

def _2__total_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "total"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c1,c2
    
    if a!=nb.nan:
        return a
    else:
        return b


############################## _3__total_f2 #####################################

def _3__total_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "total"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c3,c4
    
    if a!=nb.nan:
        return a
    else:
        return b

############################## _4__total_f2_m2_ratio #############################

def _4__total_f2_m2_ratio(row):
    m2 = row['_2__total_m2']
    f2 = row['_3__total_f2']
    return isnum(f2)/isnum(m2)


############################## _5__total_f2_m2_ratio_divergence ################

def _5__total_f2_m2_ratio_divergence(row):
    ratio = row['_4__total_f2_m2_ratio']
    return div(ratio)


############################## _6__total_test_1 ################

def _6__total_test_1(row):
    divergence = row['_5__total_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## _7__total_f2_m2_inverse_ratio #################


def _7__total_f2_m2_inverse_ratio(row):
    ratio = row['_4__total_f2_m2_ratio']
    return 1.0000001/(ratio+0.00000001)

############################## _8__total_f2_m2_inverse_ratio_divergence #################


def _8__total_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_7__total_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _9__total_test_2 #################


def _9__total_test_2(row):
    inverse_divergence = row['_8__total_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+1*8 = 9
############################## BASEMENT FUNCTIONS #####################################
############################## BASEMENT FUNCTIONS #####################################
############################## BASEMENT FUNCTIONS #####################################


############################## _10__basement_m2 #####################################

def _10__basement_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "basement"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c1,c2
    
    if a!=nb.nan:
        return a
    else:
        return b


############################## _11__basement_f2 #####################################

def _11__basement_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "basement"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c3,c4
    
    if a!=nb.nan:
        return a
    else:
        return b

############################## _12__basement_f2_m2_ratio #############################

def _12__basement_f2_m2_ratio(row):
    m2 = row['_10__basement_m2']
    f2 = row['_11__basement_f2']
    return isnum(f2)/isnum(m2)


############################## _13__basement_f2_m2_ratio_divergence ################

def _13__basement_f2_m2_ratio_divergence(row):
    ratio = row['_12__basement_f2_m2_ratio']
    return div(ratio)


############################## _14__basement_test_1 ################

def _14__basement_test_1(row):
    divergence = row['_13__basement_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## _15__basement_f2_m2_inverse_ratio #################


def _15__basement_f2_m2_inverse_ratio(row):
    ratio = row['_12__basement_f2_m2_ratio']
    return 1/ratio

############################## _16__basement_f2_m2_inverse_ratio_divergence #################


def _16__basement_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_15__basement_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _17__basement_test_2 #################


def _17__basement_test_2(row):
    inverse_divergence = row['_16__basement_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)


#1+2*8 = 17
############################## GROUND FUNCTIONS #####################################
############################## GROUND FUNCTIONS #####################################
############################## GROUND FUNCTIONS #####################################


############################## _18__ground_m2 #####################################

def _18__ground_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "ground"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c1,c2
    
    if a!=nb.nan:
        return a
    else:
        return b


############################## _19__ground_f2 #####################################

def _19__ground_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "ground"
    
    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"
    
    c1=case(string, pattern1m2.replace("word", word))
    c2=case(string, pattern2m2.replace("word", word))
    c3=case(string, pattern1f2.replace("word", word))
    c4=case(string, pattern2f2.replace("word", word))
    
    a,b=c3,c4
    
    if a!=nb.nan:
        return a
    else:
        return b

############################## _20__ground_f2_m2_ratio #############################

def _20__ground_f2_m2_ratio(row):
    m2 = row['_18__ground_m2']
    f2 = row['_19__ground_f2']
    return isnum(f2)/isnum(m2)


############################## _21__ground_f2_m2_ratio_divergence ################

def _21__ground_f2_m2_ratio_divergence(row):
    ratio = row['_20__ground_f2_m2_ratio']
    return div(ratio)


############################## _22__ground_test_1 ################

def _22__ground_test_1(row):
    divergence = row['_21__ground_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## _23__ground_f2_m2_inverse_ratio #################


def _23__ground_f2_m2_inverse_ratio(row):
    ratio = row['_20__ground_f2_m2_ratio']
    return 1/ratio

############################## _24__ground_f2_m2_inverse_ratio_divergence #################


def _24__ground_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_23__ground_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _25__ground_test_2 #################


def _25__ground_test_2(row):
    inverse_divergence = row['_24__ground_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+3*8 = 25
############################## FIRST FUNCTIONS #####################################
############################## FIRST FUNCTIONS #####################################
############################## FIRST FUNCTIONS #####################################


############################## _26__first_m2 #####################################

def _26__first_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "first"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _27__first_f2 #####################################

def _27__first_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "first"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _28__first_f2_m2_ratio #############################

def _28__first_f2_m2_ratio(row):
    m2 = row['_26__first_m2']
    f2 = row['_27__first_f2']
    return isnum(f2) / isnum(m2)


############################## _29__first_f2_m2_ratio_divergence ################

def _29__first_f2_m2_ratio_divergence(row):
    ratio = row['_28__first_f2_m2_ratio']
    return div(ratio)


############################## _30__first_test_1 ################

def _30__first_test_1(row):
    divergence = row['_29__first_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _31__first_f2_m2_inverse_ratio #################


def _31__first_f2_m2_inverse_ratio(row):
    ratio = row['_28__first_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _32__first_f2_m2_inverse_ratio_divergence #################


def _32__first_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_31__first_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _33__first_test_2 #################


def _33__first_test_2(row):
    inverse_divergence = row['_32__first_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)


#1+4*8 = 33
############################## SECOND FUNCTIONS #####################################
############################## SECOND FUNCTIONS #####################################
############################## SECOND FUNCTIONS #####################################


############################## _34__second_m2 #####################################

def _34__second_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "second"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _35__second_f2 #####################################

def _35__second_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "second"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _36__second_f2_m2_ratio #############################

def _36__second_f2_m2_ratio(row):
    m2 = row['_34__second_m2']
    f2 = row['_35__second_f2']
    return isnum(f2) / isnum(m2)


############################## _37__second_f2_m2_ratio_divergence ################

def _37__second_f2_m2_ratio_divergence(row):
    ratio = row['_36__second_f2_m2_ratio']
    return div(ratio)


############################## _38__second_test_1 ################

def _38__second_test_1(row):
    divergence = row['_37__second_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _39__second_f2_m2_inverse_ratio #################


def _39__second_f2_m2_inverse_ratio(row):
    ratio = row['_36__second_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _40__second_f2_m2_inverse_ratio_divergence #################


def _40__second_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_39__second_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _41__second_test_2 #################


def _41__second_test_2(row):
    inverse_divergence = row['_40__second_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+5*8 = 41
############################## THIRD FUNCTIONS #####################################
############################## THIRD FUNCTIONS #####################################
############################## THIRD FUNCTIONS #####################################


############################## _42__third_m2 #####################################

def _42__third_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "third"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _43__third_f2 #####################################

def _43__third_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "third"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _44__third_f2_m2_ratio #############################

def _44__third_f2_m2_ratio(row):
    m2 = row['_42__third_m2']
    f2 = row['_43__third_f2']
    return isnum(f2) / isnum(m2)


############################## _45__third_f2_m2_ratio_divergence ################

def _45__third_f2_m2_ratio_divergence(row):
    ratio = row['_44__third_f2_m2_ratio']
    return div(ratio)


############################## _46__third_test_1 ################

def _46__third_test_1(row):
    divergence = row['_45__third_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _47__third_f2_m2_inverse_ratio #################


def _47__third_f2_m2_inverse_ratio(row):
    ratio = row['_44__third_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _48__third_f2_m2_inverse_ratio_divergence #################


def _48__third_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_47__third_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _49__third_test_2 #################


def _49__third_test_2(row):
    inverse_divergence = row['_48__third_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+6*8 = 49
############################## FOURTH FUNCTIONS #####################################
############################## FOURTH FUNCTIONS #####################################
############################## FOURTH FUNCTIONS #####################################


############################## _50__fourth_m2 #####################################

def _50__fourth_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "fourth"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _51__fourth_f2 #####################################

def _51__fourth_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "fourth"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _52__fourth_f2_m2_ratio #############################

def _52__fourth_f2_m2_ratio(row):
    m2 = row['_50__fourth_m2']
    f2 = row['_51__fourth_f2']
    return isnum(f2) / isnum(m2)


############################## _53__fourth_f2_m2_ratio_divergence ################

def _53__fourth_f2_m2_ratio_divergence(row):
    ratio = row['_52__fourth_f2_m2_ratio']
    return div(ratio)


############################## _54__fourth_test_1 ################

def _54__fourth_test_1(row):
    divergence = row['_53__fourth_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _55__fourth_f2_m2_inverse_ratio #################


def _55__fourth_f2_m2_inverse_ratio(row):
    ratio = row['_52__fourth_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _56__fourth_f2_m2_inverse_ratio_divergence #################


def _56__fourth_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_55__fourth_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _57__fourth_test_2 #################


def _57__fourth_test_2(row):
    inverse_divergence = row['_56__fourth_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+7*8 = 57
############################## FIFTH FUNCTIONS ####################################
############################## FIFTH FUNCTIONS ####################################
############################## FIFTH FUNCTIONS ####################################


############################## _58__fifth_m2 #####################################

def _58__fifth_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "fifth"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _59__fifth_f2 #####################################

def _59__fifth_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "fifth"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _60__fifth_f2_m2_ratio #############################

def _60__fifth_f2_m2_ratio(row):
    m2 = row['_58__fifth_m2']
    f2 = row['_59__fifth_f2']
    return isnum(f2) / isnum(m2)


############################## _61__fifth_f2_m2_ratio_divergence ################

def _61__fifth_f2_m2_ratio_divergence(row):
    ratio = row['_60__fifth_f2_m2_ratio']
    return div(ratio)


############################## _62__fifth_test_1 ################

def _62__fifth_test_1(row):
    divergence = row['_61__fifth_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _63__fifth_f2_m2_inverse_ratio #################


def _63__fifth_f2_m2_inverse_ratio(row):
    ratio = row['_60__fifth_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _64__fifth_f2_m2_inverse_ratio_divergence #################


def _64__fifth_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_63__fifth_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _65__fifth_test_2 #################


def _65__fifth_test_2(row):
    inverse_divergence = row['_64__fifth_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)


#1+8*8 = 65
############################## CELLAR FUNCTIONS ####################################
############################## CELLAR FUNCTIONS ####################################
############################## CELLAR FUNCTIONS ####################################


############################## _66__cellar_m2 #####################################

def _66__cellar_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "cellar"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _67__cellar_f2 #####################################

def _67__cellar_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "cellar"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _68__cellar_f2_m2_ratio #############################

def _68__cellar_f2_m2_ratio(row):
    m2 = row['_66__cellar_m2']
    f2 = row['_67__cellar_f2']
    return isnum(f2) / isnum(m2)


############################## _69__cellar_f2_m2_ratio_divergence ################

def _69__cellar_f2_m2_ratio_divergence(row):
    ratio = row['_68__cellar_f2_m2_ratio']
    return div(ratio)


############################## _70__cellar_test_1 ################

def _70__cellar_test_1(row):
    divergence = row['_69__cellar_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _71__cellar_f2_m2_inverse_ratio #################


def _71__cellar_f2_m2_inverse_ratio(row):
    ratio = row['_68__cellar_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _72__cellar_f2_m2_inverse_ratio_divergence #################


def _72__cellar_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_71__cellar_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _73__cellar_test_2 #################


def _73__cellar_test_2(row):
    inverse_divergence = row['_72__cellar_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+9*8 = 73
############################## GARAGE FUNCTIONS ####################################
############################## GARAGE FUNCTIONS ####################################
############################## GARAGE FUNCTIONS ####################################

############################## _74__garage_m2 #####################################

def _74__garage_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "garage"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _75__garage_f2 #####################################

def _75__garage_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "garage"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _76__garage_f2_m2_ratio #############################

def _76__garage_f2_m2_ratio(row):
    m2 = row['_74__garage_m2']
    f2 = row['_75__garage_f2']
    return isnum(f2) / isnum(m2)


############################## _77__garage_f2_m2_ratio_divergence ################

def _77__garage_f2_m2_ratio_divergence(row):
    ratio = row['_76__garage_f2_m2_ratio']
    return div(ratio)


############################## _78__garage_test_1 ################

def _78__garage_test_1(row):
    divergence = row['_77__garage_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _79__garage_f2_m2_inverse_ratio #################


def _79__garage_f2_m2_inverse_ratio(row):
    ratio = row['_76__garage_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _80__garage_f2_m2_inverse_ratio_divergence #################


def _80__garage_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_79__garage_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _81__garage_test_2 #################


def _81__garage_test_2(row):
    inverse_divergence = row['_80__garage_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)


#1+10*8 = 81
############################## MEZZANINE FUNCTIONS ####################################
############################## MEZZANINE FUNCTIONS ####################################
############################## MEZZANINE FUNCTIONS ####################################


############################## _82__mezzanine_m2 #####################################

def _82__mezzanine_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "mezzanine"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _83__mezzanine_f2 #####################################

def _83__mezzanine_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "mezzanine"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _84__mezzanine_f2_m2_ratio #############################

def _84__mezzanine_f2_m2_ratio(row):
    m2 = row['_82__mezzanine_m2']
    f2 = row['_83__mezzanine_f2']
    return isnum(f2) / isnum(m2)


############################## _85__mezzanine_f2_m2_ratio_divergence ################

def _85__mezzanine_f2_m2_ratio_divergence(row):
    ratio = row['_84__mezzanine_f2_m2_ratio']
    return div(ratio)


############################## _86__mezzanine_test_1 ################

def _86__mezzanine_test_1(row):
    divergence = row['_85__mezzanine_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _87__mezzanine_f2_m2_inverse_ratio #################


def _87__mezzanine_f2_m2_inverse_ratio(row):
    ratio = row['_84__mezzanine_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _88__mezzanine_f2_m2_inverse_ratio_divergence #################


def _88__mezzanine_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_87__mezzanine_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _89__mezzanine_test_2 #################


def _89__mezzanine_test_2(row):
    inverse_divergence = row['_88__mezzanine_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+11*8 = 89
############################## OUTBUILDINGS FUNCTIONS ####################################
############################## OUTBUILDINGS FUNCTIONS ####################################
############################## OUTBUILDINGS FUNCTIONS ####################################


############################## _90__outbuildings_m2 #####################################

def _90__outbuildings_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "outbuildings"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _91__outbuildings_f2 #####################################

def _91__outbuildings_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "outbuildings"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _92__outbuildings_f2_m2_ratio #############################

def _92__outbuildings_f2_m2_ratio(row):
    m2 = row['_90__outbuildings_m2']
    f2 = row['_91__outbuildings_f2']
    return isnum(f2) / isnum(m2)


############################## _93__outbuildings_f2_m2_ratio_divergence ################

def _93__outbuildings_f2_m2_ratio_divergence(row):
    ratio = row['_92__outbuildings_f2_m2_ratio']
    return div(ratio)


############################## _94__outbuildings_test_1 ################

def _94__outbuildings_test_1(row):
    divergence = row['_93__outbuildings_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _95__outbuildings_f2_m2_inverse_ratio #################


def _95__outbuildings_f2_m2_inverse_ratio(row):
    ratio = row['_92__outbuildings_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _96__outbuildings_f2_m2_inverse_ratio_divergence #################


def _96__outbuildings_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_95__outbuildings_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _97__outbuildings_test_2 #################


def _97__outbuildings_test_2(row):
    inverse_divergence = row['_96__outbuildings_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+12*8 = 97
############################## BALCONY FUNCTIONS ####################################
############################## BALCONY FUNCTIONS ####################################
############################## BALCONY FUNCTIONS ####################################


############################## _98__balcony_m2 #####################################

def _98__balcony_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "balcony"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _99__balcony_f2 #####################################

def _99__balcony_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "balcony"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _100__balcony_f2_m2_ratio #############################

def _100__balcony_f2_m2_ratio(row):
    m2 = row['_98__balcony_m2']
    f2 = row['_99__balcony_f2']
    return isnum(f2) / isnum(m2)


############################## _101__balcony_f2_m2_ratio_divergence ################

def _101__balcony_f2_m2_ratio_divergence(row):
    ratio = row['_100__balcony_f2_m2_ratio']
    return div(ratio)


############################## _102__balcony_test_1 ################

def _102__balcony_test_1(row):
    divergence = row['_101__balcony_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _103__balcony_f2_m2_inverse_ratio #################


def _103__balcony_f2_m2_inverse_ratio(row):
    ratio = row['_100__balcony_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _104__balcony_f2_m2_inverse_ratio_divergence #################


def _104__balcony_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_103__balcony_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _105__balcony_test_2 #################


def _105__balcony_test_2(row):
    inverse_divergence = row['_104__balcony_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+13*8 = 98
############################## LIMITED USE FUNCTIONS ####################################
############################## LIMITED USE FUNCTIONS ####################################
############################## LIMITED USE FUNCTIONS ####################################


############################## _106__limited_use_m2 #####################################

def _106__limited_use_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "limited use"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _107__limited_use_f2 #####################################

def _107__limited_use_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "limited use"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _108__limited_use_f2_m2_ratio #############################

def _108__limited_use_f2_m2_ratio(row):
    m2 = row['_106__limited_use_m2']
    f2 = row['_107__limited_use_f2']
    return isnum(f2) / isnum(m2)


############################## _109__limited_use_f2_m2_ratio_divergence ################

def _109__limited_use_f2_m2_ratio_divergence(row):
    ratio = row['_108__limited_use_f2_m2_ratio']
    return div(ratio)


############################## _110__limited_use_test_1 ################

def _110__limited_use_test_1(row):
    divergence = row['_109__limited_use_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _111__limited_use_f2_m2_inverse_ratio #################


def _111__limited_use_f2_m2_inverse_ratio(row):
    ratio = row['_108__limited_use_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _112__limited_use_f2_m2_inverse_ratio_divergence #################


def _112__limited_use_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_111__limited_use_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _113__limited_use_test_2 #################


def _113__limited_use_test_2(row):
    inverse_divergence = row['_112__limited_use_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+14*8 = 113
############################## VOIDS FUNCTIONS ####################################
############################## VOIDS FUNCTIONS ####################################
############################## VOIDS FUNCTIONS ####################################


############################## _114__voids_m2 #####################################

def _114__voids_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "voids"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _115__voids_f2 #####################################

def _115__voids_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "voids"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _116__voids_f2_m2_ratio #############################

def _116__voids_f2_m2_ratio(row):
    m2 = row['_114__voids_m2']
    f2 = row['_115__voids_f2']
    return isnum(f2) / isnum(m2)


############################## _117__voids_f2_m2_ratio_divergence ################

def _117__voids_f2_m2_ratio_divergence(row):
    ratio = row['_116__voids_f2_m2_ratio']
    return div(ratio)


############################## _118__voids_test_1 ################

def _118__voids_test_1(row):
    divergence = row['_117__voids_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _119__voids_f2_m2_inverse_ratio #################


def _119__voids_f2_m2_inverse_ratio(row):
    ratio = row['_116__voids_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _120__voids_f2_m2_inverse_ratio_divergence #################


def _120__voids_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_119__voids_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _121__voids_test_2 #################


def _121__voids_test_2(row):
    inverse_divergence = row['_120__voids_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+15*8 = 121
############################## RESTRICTED HEIGHT FUNCTIONS ####################################
############################## RESTRICTED HEIGHT FUNCTIONS ####################################
############################## RESTRICTED HEIGHT FUNCTIONS ####################################


############################## _122__restricted_height_m2 #####################################

def _122__restricted_height_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "restricted height"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _123__restricted_height_f2 #####################################

def _123__restricted_height_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "restricted height"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _124__restricted_height_f2_m2_ratio #############################

def _124__restricted_height_f2_m2_ratio(row):
    m2 = row['_122__restricted_height_m2']
    f2 = row['_123__restricted_height_f2']
    return isnum(f2) / isnum(m2)


############################## _125__restricted_height_f2_m2_ratio_divergence ################

def _125__restricted_height_f2_m2_ratio_divergence(row):
    ratio = row['_124__restricted_height_f2_m2_ratio']
    return div(ratio)


############################## _126__restricted_height_test_1 ################

def _126__restricted_height_test_1(row):
    divergence = row['_125__restricted_height_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _127__restricted_height_f2_m2_inverse_ratio #################


def _127__restricted_height_f2_m2_inverse_ratio(row):
    ratio = row['_124__restricted_height_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _128__restricted_height_f2_m2_inverse_ratio_divergence #################


def _128__restricted_height_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_127__restricted_height_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _129__restricted_height_test_2 #################


def _129__restricted_height_test_2(row):
    inverse_divergence = row['_128__restricted_height_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+16*8 = 129
############################## CONSERVATORY FUNCTIONS ####################################
############################## CONSERVATORY FUNCTIONS ####################################
############################## CONSERVATORY FUNCTIONS ####################################


############################## _130__conservatory_m2 #####################################

def _130__conservatory_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "conservatory"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _131__conservatory_f2 #####################################

def _131__conservatory_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "conservatory"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _132__conservatory_f2_m2_ratio #############################

def _132__conservatory_f2_m2_ratio(row):
    m2 = row['_130__conservatory_m2']
    f2 = row['_131__conservatory_f2']
    return isnum(f2) / isnum(m2)


############################## _133__conservatory_f2_m2_ratio_divergence ################

def _133__conservatory_f2_m2_ratio_divergence(row):
    ratio = row['_132__conservatory_f2_m2_ratio']
    return div(ratio)


############################## _134__conservatory_test_1 ################

def _134__conservatory_test_1(row):
    divergence = row['_133__conservatory_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _135__conservatory_f2_m2_inverse_ratio #################


def _135__conservatory_f2_m2_inverse_ratio(row):
    ratio = row['_132__conservatory_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _136__conservatory_f2_m2_inverse_ratio_divergence #################


def _136__conservatory_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_135__conservatory_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _137__conservatory_test_2 #################


def _137__conservatory_test_2(row):
    inverse_divergence = row['_136__conservatory_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)

#1+17*8 = 137
############################## COTTAGE FUNCTIONS ####################################
############################## COTTAGE FUNCTIONS ####################################
############################## COTTAGE FUNCTIONS ####################################


############################## _138__cottage_m2 #####################################

def _138__cottage_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "cottage"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _139__cottage_f2 #####################################

def _139__cottage_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "cottage"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _140__cottage_f2_m2_ratio #############################

def _140__cottage_f2_m2_ratio(row):
    m2 = row['_138__cottage_m2']
    f2 = row['_139__cottage_f2']
    return isnum(f2) / isnum(m2)


############################## _141__cottage_f2_m2_ratio_divergence ################

def _141__cottage_f2_m2_ratio_divergence(row):
    ratio = row['_140__cottage_f2_m2_ratio']
    return div(ratio)


############################## _142__cottage_test_1 ################

def _142__cottage_test_1(row):
    divergence = row['_141__cottage_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _143__cottage_f2_m2_inverse_ratio #################


def _143__cottage_f2_m2_inverse_ratio(row):
    ratio = row['_140__cottage_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _144__cottage_f2_m2_inverse_ratio_divergence #################


def _144__cottage_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_143__cottage_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _145__cottage_test_2 #################


def _145__cottage_test_2(row):
    inverse_divergence = row['_144__cottage_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)





#1+18*8 = 145
############################## MAIN HOUSE FUNCTIONS ####################################
############################## MAIN HOUSE FUNCTIONS ####################################
############################## MAIN HOUSE FUNCTIONS ####################################


############################## _146__main_house_m2 #####################################

def _146__main_house_m2(row):
    string = row["ocr_concatenation"].lower()
    word = "main house"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c1, c2

    if a != nb.nan:
        return a
    else:
        return b


############################## _147__main_house_f2 #####################################

def _147__main_house_f2(row):
    string = row["ocr_concatenation"].lower()
    word = "main house"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> m2
    pattern1m2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*m.*\d{1,6}\.?\d{0,4}.*f)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> m2
    pattern2m2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*f.*)\d{1,6}\.?\d{0,4}(?=.*m)"

    # word ~~ f2:NumberString ~~ 'f' ~~ m2:NumberString ~~ 'm' :> f2
    pattern1f2 = r"(?<=word.*)\d{1,6}\.?\d{0,4}(?=.*f.*\d{1,6}\.?\d{0,4}.*m)"

    # word ~~ m2:NumberString ~~ 'm' ~~ f2:NumberString ~~ 'f' :> f2
    pattern2f2 = r"(?<=word.*\d{1,6}\.?\d{0,4}.*m.*)\d{1,6}\.?\d{0,4}(?=.*f)"

    c1 = case(string, pattern1m2.replace("word", word))
    c2 = case(string, pattern2m2.replace("word", word))
    c3 = case(string, pattern1f2.replace("word", word))
    c4 = case(string, pattern2f2.replace("word", word))

    a, b = c3, c4

    if a != nb.nan:
        return a
    else:
        return b


############################## _148__main_house_f2_m2_ratio #############################

def _148__main_house_f2_m2_ratio(row):
    m2 = row['_146__main_house_m2']
    f2 = row['_147__main_house_f2']
    return isnum(f2) / isnum(m2)


############################## _149__main_house_f2_m2_ratio_divergence ################

def _149__main_house_f2_m2_ratio_divergence(row):
    ratio = row['_148__main_house_f2_m2_ratio']
    return div(ratio)


############################## _150__main_house_test_1 ################

def _150__main_house_test_1(row):
    divergence = row['_149__main_house_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _151__main_house_f2_m2_inverse_ratio #################


def _151__main_house_f2_m2_inverse_ratio(row):
    ratio = row['_148__main_house_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _152__main_house_f2_m2_inverse_ratio_divergence #################


def _152__main_house_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_151__main_house_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _153__main_house_test_2 #################


def _153__main_house_test_2(row):
    inverse_divergence = row['_152__main_house_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)












#1+19*8 = 153
############################## GRAND TOTAL FUNCTIONS ####################################
############################## GRAND TOTAL FUNCTIONS ####################################
############################## GRAND TOTAL FUNCTIONS ####################################


############################## _154__grand_total_m2 #####################################

def _154__grand_total_m2(row):
    l = [
        '_10__basement_m2',
        '_18__ground_m2',
        '_26__first_m2',
        '_34__second_m2',
        '_42__third_m2',
        '_50__fourth_m2',
        '_58__fifth_m2',
        '_66__cellar_m2',
        '_74__garage_m2',
        '_82__mezzanine_m2',
        '_90__outbuildings_m2',
        '_98__balcony_m2',
        '_106__limited_use_m2',
        '_114__voids_m2',
        '_122__restricted_height_m2',
        '_130__conservatory_m2',
        '_138__cottage_m2',
        '_146__main_house_m2'
    ]
    q=0
    for i in l:
        if row[i]!='.':
            q=q + row[i]
        else:
            q =q+0
    return q


############################## _155__grand_total_f2 #####################################

def _155__grand_total_f2(row):
    l = [
        '_11__basement_f2',
        '_19__ground_f2',
        '_27__first_f2',
        '_35__second_f2',
        '_43__third_f2',
        '_51__fourth_f2',
        '_59__fifth_f2',
        '_67__cellar_f2',
        '_75__garage_f2',
        '_83__mezzanine_f2',
        '_91__outbuildings_f2',
        '_99__balcony_f2',
        '_107__limited_use_f2',
        '_115__voids_f2',
        '_123__restricted_height_f2',
        '_131__conservatory_f2',
        '_139__cottage_f2',
        '_147__main_house_f2'
    ]
    q=0
    for i in l:
        if row[i] != '.':
            q = q + row[i]
        else:
            q = q + 0
    return q



############################## _156__grand_total_f2_m2_ratio #############################

def _156__grand_total_f2_m2_ratio(row):
    m2 = row['_154__grand_total_m2']
    f2 = row['_155__grand_total_f2']
    return isnum(f2) / isnum(m2)


############################## _157__grand_total_f2_m2_ratio_divergence ################

def _157__grand_total_f2_m2_ratio_divergence(row):
    ratio = row['_156__grand_total_f2_m2_ratio']
    return div(ratio)


############################## _158__grand_total_test_1 ################

def _158__grand_total_test_1(row):
    divergence = row['_157__grand_total_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _159__grand_total_f2_m2_inverse_ratio #################


def _159__grand_total_f2_m2_inverse_ratio(row):
    ratio = row['_156__grand_total_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _160__grand_total_f2_m2_inverse_ratio_divergence #################


def _160__grand_total_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_159__grand_total_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _161__grand_total_test_2 #################


def _161__grand_total_test_2(row):
    inverse_divergence = row['_160__grand_total_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)












#1+20*8 = 161
############################## INTERNAL TOTAL FUNCTIONS ####################################
############################## INTERNAL TOTAL FUNCTIONS ####################################
############################## INTERNAL TOTAL FUNCTIONS ####################################


############################## _162__internal_total_m2 #####################################

def _162__internal_total_m2(row):
    l = [
        '_10__basement_m2',
        '_18__ground_m2',
        '_26__first_m2',
        '_34__second_m2',
        '_42__third_m2',
        '_50__fourth_m2',
        '_58__fifth_m2',
        '_66__cellar_m2',
        '_74__garage_m2',
        '_82__mezzanine_m2',
        '_90__outbuildings_m2',
        '_98__balcony_m2',
        '_106__limited_use_m2',
        '_114__voids_m2',
        '_122__restricted_height_m2',
        '_130__conservatory_m2',
        '_138__cottage_m2',
        '_146__main_house_m2'
    ]
    q=0
    for i in l:
        if row[i]!='.':
            q=q + row[i]
        else:
            q =q+0
    return q


############################## _163__internal_total_f2 #####################################

def _163__internal_total_f2(row):
    l = [
        '_11__basement_f2',
        '_19__ground_f2',
        '_27__first_f2',
        '_35__second_f2',
        '_43__third_f2',
        '_51__fourth_f2',
        '_59__fifth_f2',
        '_67__cellar_f2',
        '_75__garage_f2',
        '_83__mezzanine_f2',
        '_91__outbuildings_f2',
        '_99__balcony_f2',
        '_107__limited_use_f2',
        '_115__voids_f2',
        '_123__restricted_height_f2',
        '_131__conservatory_f2',
        '_139__cottage_f2',
        '_147__main_house_f2'
    ]
    q=0
    for i in l:
        if row[i] != '.':
            q = q + row[i]
        else:
            q = q + 0
    return q



############################## _164__internal_total_f2_m2_ratio #############################

def _164__internal_total_f2_m2_ratio(row):
    m2 = row['_162__internal_total_m2']
    f2 = row['_163__internal_total_f2']
    return isnum(f2) / isnum(m2)


############################## _165__internal_total_f2_m2_ratio_divergence ################

def _165__internal_total_f2_m2_ratio_divergence(row):
    ratio = row['_164__internal_total_f2_m2_ratio']
    return div(ratio)


############################## _166__internal_total_test_1 ################

def _166__internal_total_test_1(row):
    divergence = row['_165__internal_total_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _167__internal_total_f2_m2_inverse_ratio #################


def _167__internal_total_f2_m2_inverse_ratio(row):
    ratio = row['_164__internal_total_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _168__internal_total_f2_m2_inverse_ratio_divergence #################


def _168__internal_total_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_167__internal_total_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _169__internal_total_test_2 #################


def _169__internal_total_test_2(row):
    inverse_divergence = row['_168__internal_total_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)












#1+21*8 = 169
############################## EXTERNAL TOTAL FUNCTIONS ####################################
############################## EXTERNAL TOTAL FUNCTIONS ####################################
############################## EXTERNAL TOTAL FUNCTIONS ####################################


############################## _170__external_total_m2 #####################################

def _170__external_total_m2(row):
    l = [
        '_10__basement_m2',
        '_18__ground_m2',
        '_26__first_m2',
        '_34__second_m2',
        '_42__third_m2',
        '_50__fourth_m2',
        '_58__fifth_m2',
        '_66__cellar_m2',
        '_74__garage_m2',
        '_82__mezzanine_m2',
        '_90__outbuildings_m2',
        '_98__balcony_m2',
        '_106__limited_use_m2',
        '_114__voids_m2',
        '_122__restricted_height_m2',
        '_130__conservatory_m2',
        '_138__cottage_m2',
        '_146__main_house_m2'
    ]
    q=0
    for i in l:
        if row[i]!='.':
            q=q + row[i]
        else:
            q =q+0
    return q


############################## _171__external_total_f2 #####################################

def _171__external_total_f2(row):
    l = [
        '_11__basement_f2',
        '_19__ground_f2',
        '_27__first_f2',
        '_35__second_f2',
        '_43__third_f2',
        '_51__fourth_f2',
        '_59__fifth_f2',
        '_67__cellar_f2',
        '_75__garage_f2',
        '_83__mezzanine_f2',
        '_91__outbuildings_f2',
        '_99__balcony_f2',
        '_107__limited_use_f2',
        '_115__voids_f2',
        '_123__restricted_height_f2',
        '_131__conservatory_f2',
        '_139__cottage_f2',
        '_147__main_house_f2'
    ]
    q=0
    for i in l:
        if row[i] != '.':
            q = q + row[i]
        else:
            q = q + 0
    return q



############################## _172__external_total_f2_m2_ratio #############################

def _172__external_total_f2_m2_ratio(row):
    m2 = row['_170__external_total_m2']
    f2 = row['_171__external_total_f2']
    return isnum(f2) / isnum(m2)


############################## _173__external_total_f2_m2_ratio_divergence ################

def _173__external_total_f2_m2_ratio_divergence(row):
    ratio = row['_172__external_total_f2_m2_ratio']
    return div(ratio)


############################## _174__external_total_test_1 ################

def _174__external_total_test_1(row):
    divergence = row['_173__external_total_f2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## _175__external_total_f2_m2_inverse_ratio #################


def _175__external_total_f2_m2_inverse_ratio(row):
    ratio = row['_172__external_total_f2_m2_ratio']
    return 1.00001 / (ratio + 0.00001)


############################## _176__external_total_f2_m2_inverse_ratio_divergence #################


def _176__external_total_f2_m2_inverse_ratio_divergence(row):
    inverse_ratio = row['_175__external_total_f2_m2_inverse_ratio']
    return div(inverse_ratio)


############################## _177__external_total_test_2 #################


def _177__external_total_test_2(row):
    inverse_divergence = row['_176__external_total_f2_m2_inverse_ratio_divergence']
    return div_under_3_percent(inverse_divergence)




#1+22*8 = 177
############################## M2 FUNCTIONS ####################################
############################## M2 FUNCTIONS ####################################
############################## M2 FUNCTIONS ####################################


############################## _178__description_M2 ############################
def _178__description_M2(row):
    m = row['_005__description_m2']
    f = row['_006__description_f2']
    if not nb.isnan(m):
        return m
    elif not nb.isnan(f):
        return (f + 0.000001) * 0.09290304
    else:
        return nb.nan
############################## _179__summary_box_M2 ############################
def _179__summary_box_M2(row):
    m = row['_014__summary_box_m2']
    f = row['_015__summary_box_f2']
    if not nb.isnan(m):
        return m
    elif not nb.isnan(f):
        return (f + 0.000001) * 0.09290304
    else:
        return nb.nan

############################## _180__total_M2 ############################
def _180__total_M2(row):
    m = row['_2__total_m2']
    f = row['_3__total_f2']
    if not nb.isnan(m):
        return m
    elif not nb.isnan(f):
        return (f + 0.000001) * 0.09290304
    else:
        return nb.nan

############################## _181__grand_total_M2 ############################
def _181__grand_total_M2(row):
    m = row['_154__grand_total_m2']
    f = row['_155__grand_total_f2']
    if not nb.isnan(m):
        return m
    elif not nb.isnan(f):
        return (f + 0.000001) * 0.09290304
    else:
        return nb.nan


############################## GRAND TOTAL TOTAL FUNCTIONS #####################
############################## GRAND TOTAL TOTAL FUNCTIONS #####################
############################## GRAND TOTAL TOTAL FUNCTIONS #####################

############################## _182__grand_total_total_M2_M2_ratio ############################
def _182__grand_total_total_M2_M2_ratio(row):
    a = row['_181__grand_total_M2']
    b = row['_180__total_M2']
    return isnum(a) / isnum(b)

############################## _183__grand_total_total_m2_m2_ratio_divergence ################
def _183__grand_total_total_m2_m2_ratio_divergence(row):
    ratio = row['_182__grand_total_total_M2_M2_ratio']
    return div(ratio)
############################## _184__grand_total_total_test ################
def _184__grand_total_total_test(row):
    divergence = row['_183__grand_total_total_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## SUMMARY BOX TOTAL FUNCTIONS #####################
############################## SUMMARY BOX TOTAL FUNCTIONS #####################
############################## SUMMARY BOX TOTAL FUNCTIONS #####################

############################## _185__summary_box_total_M2_M2_ratio ############################
def _185__summary_box_total_M2_M2_ratio(row):
    a = row['_179__summary_box_M2']
    b = row['_180__total_M2']
    return isnum(a) / isnum(b)

############################## _186__summary_box_total_m2_m2_ratio_divergence ################
def _186__summary_box_total_m2_m2_ratio_divergence(row):
    ratio = row['_185__summary_box_total_M2_M2_ratio']
    return div(ratio)
############################## _187__summary_box_total_test ################
def _187__summary_box_total_test(row):
    divergence = row['_186__summary_box_total_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## SUMMARY BOX GRAND_TOTAL FUNCTIONS #####################
############################## SUMMARY BOX GRAND TOTAL FUNCTIONS #####################
############################## SUMMARY BOX GRAND TOTAL FUNCTIONS #####################

############################## _188__summary_box_grand_total_M2_M2_ratio ############################
def _188__summary_box_grand_total_M2_M2_ratio(row):
    a = row['_179__summary_box_M2']
    b = row['_181__grand_total_M2']
    return isnum(a) / isnum(b)

############################## _189__summary_box_grand_total_m2_m2_ratio_divergence ################
def _189__summary_box_grand_total_m2_m2_ratio_divergence(row):
    ratio = row['_188__summary_box_grand_total_M2_M2_ratio']
    return div(ratio)
############################## _190__summary_box_grand_total_test ################
def _190__summary_box_grand_total_test(row):
    divergence = row['_189__summary_box_grand_total_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)


############################## DESCRIPTION TOTAL FUNCTIONS #####################
############################## DESCRIPTION TOTAL FUNCTIONS #####################
############################## DESCRIPTION TOTAL FUNCTIONS #####################

############################## _191__description_total_M2_M2_ratio ############################
def _191__description_total_M2_M2_ratio(row):
    a = row['_178__description_M2']
    b = row['_180__total_M2']
    return isnum(a) / isnum(b)

############################## _192__description_total_m2_m2_ratio_divergence ################
def _192__description_total_m2_m2_ratio_divergence(row):
    ratio = row['_191__description_total_M2_M2_ratio']
    return div(ratio)
############################## _193__description_total_test ################
def _193__description_total_test(row):
    divergence = row['_192__description_total_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## DESCRIPTION GRAND_TOTAL FUNCTIONS #####################
############################## DESCRIPTION GRAND TOTAL FUNCTIONS #####################
############################## DESCRIPTION GRAND TOTAL FUNCTIONS #####################

############################## _194__description_grand_total_M2_M2_ratio ############################
def _194__description_grand_total_M2_M2_ratio(row):
    a = row['_178__description_M2']
    b = row['_181__grand_total_M2']
    return isnum(a) / isnum(b)

############################## _195__description_grand_total_m2_m2_ratio_divergence ################
def _195__description_grand_total_m2_m2_ratio_divergence(row):
    ratio = row['_194__description_grand_total_M2_M2_ratio']
    return div(ratio)
############################## _196__description_grand_total_test ################
def _196__description_grand_total_test(row):
    divergence = row['_195__description_grand_total_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

############################## DESCRIPTION SUMMARY BOX #####################
############################## DESCRIPTION SUMMARY BOX #####################
############################## DESCRIPTION SUMMARY BOX #####################

############################## _197__description_summary_box_M2_M2_ratio ############################
def _197__description_summary_box_M2_M2_ratio(row):
    a = row['_178__description_M2']
    b = row['_179__summary_box_M2']
    return isnum(a) / isnum(b)

############################## _198__description_summary_box_m2_m2_ratio_divergence ################
def _198__description_summary_box_m2_m2_ratio_divergence(row):
    ratio = row['_197__description_summary_box_M2_M2_ratio']
    return div(ratio)
############################## _199__description_summary_box_test ################
def _199__description_summary_box_test(row):
    divergence = row['_198__description_summary_box_m2_m2_ratio_divergence']
    return div_under_3_percent(divergence)

# we need to edit the image.csv tables to make multiple columns per id number

def _200__high_confidence_level_hit(row):
    l = [
        row['_184__grand_total_total_test'],
        row['_187__summary_box_total_test'],
        row['_190__summary_box_grand_total_test'],
        row['_193__description_total_test'],
        row['_196__description_grand_total_test'],
        row['_199__description_summary_box_test']
    ]
    return all(l)




####################C COLUMNS ##########################################################


columns=[
    '_004__description',
    '_005__description_m2',
    '_006__description_f2',
    '_007__description_f2_m2_ratio',
    '_008__description_f2_m2_ratio_divergence',
    '_009__description_test_1',
    '_010__description_f2_m2_inverse_ratio',
    '_011__description_f2_m2_inverse_ratio_divergence',
    '_012__description_test_2',
    '_014__summary_box_m2',
    '_015__summary_box_f2',
    '_016__summary_box_f2_m2_ratio',
    '_017__summary_box_f2_m2_ratio_divergence',
    '_018__summary_box_test_1',
    '_019__summary_box_f2_m2_inverse_ratio',
    '_020__summary_box_f2_m2_inverse_ratio_divergence',
    '_021__summary_box_test_2',
    '_2__total_m2',
    '_3__total_f2',
    '_4__total_f2_m2_ratio',
    '_5__total_f2_m2_ratio_divergence',
    '_6__total_test_1',
    '_7__total_f2_m2_inverse_ratio',
    '_8__total_f2_m2_inverse_ratio_divergence',
    '_9__total_test_2',
    '_10__basement_m2',
    '_11__basement_f2',
    '_12__basement_f2_m2_ratio',
    '_13__basement_f2_m2_ratio_divergence',
    '_14__basement_test_1',
    '_15__basement_f2_m2_inverse_ratio',
    '_16__basement_f2_m2_inverse_ratio_divergence',
    '_17__basement_test_2',
    '_18__ground_m2',
    '_19__ground_f2',
    '_20__ground_f2_m2_ratio',
    '_21__ground_f2_m2_ratio_divergence',
    '_22__ground_test_1',
    '_23__ground_f2_m2_inverse_ratio',
    '_24__ground_f2_m2_inverse_ratio_divergence',
    '_25__ground_test_2',
    '_26__first_m2',
    '_27__first_f2',
    '_28__first_f2_m2_ratio',
    '_29__first_f2_m2_ratio_divergence',
    '_30__first_test_1',
    '_31__first_f2_m2_inverse_ratio',
    '_32__first_f2_m2_inverse_ratio_divergence',
    '_33__first_test_2',
    '_34__second_m2',
    '_35__second_f2',
    '_36__second_f2_m2_ratio',
    '_37__second_f2_m2_ratio_divergence',
    '_38__second_test_1',
    '_39__second_f2_m2_inverse_ratio',
    '_40__second_f2_m2_inverse_ratio_divergence',
    '_41__second_test_2',
    '_42__third_m2',
    '_43__third_f2',
    '_44__third_f2_m2_ratio',
    '_45__third_f2_m2_ratio_divergence',
    '_46__third_test_1',
    '_47__third_f2_m2_inverse_ratio',
    '_48__third_f2_m2_inverse_ratio_divergence',
    '_49__third_test_2',
    '_50__fourth_m2',
    '_51__fourth_f2',
    '_52__fourth_f2_m2_ratio',
    '_53__fourth_f2_m2_ratio_divergence',
    '_54__fourth_test_1',
    '_55__fourth_f2_m2_inverse_ratio',
    '_56__fourth_f2_m2_inverse_ratio_divergence',
    '_57__fourth_test_2',
    '_58__fifth_m2',
    '_59__fifth_f2',
    '_60__fifth_f2_m2_ratio',
    '_61__fifth_f2_m2_ratio_divergence',
    '_62__fifth_test_1',
    '_63__fifth_f2_m2_inverse_ratio',
    '_64__fifth_f2_m2_inverse_ratio_divergence',
    '_65__fifth_test_2',
    '_66__cellar_m2',
    '_67__cellar_f2',
    '_68__cellar_f2_m2_ratio',
    '_69__cellar_f2_m2_ratio_divergence',
    '_70__cellar_test_1',
    '_71__cellar_f2_m2_inverse_ratio',
    '_72__cellar_f2_m2_inverse_ratio_divergence',
    '_73__cellar_test_2',
    '_74__garage_m2',
    '_75__garage_f2',
    '_76__garage_f2_m2_ratio',
    '_77__garage_f2_m2_ratio_divergence',
    '_78__garage_test_1',
    '_79__garage_f2_m2_inverse_ratio',
    '_80__garage_f2_m2_inverse_ratio_divergence',
    '_81__garage_test_2',
    '_82__mezzanine_m2',
    '_83__mezzanine_f2',
    '_84__mezzanine_f2_m2_ratio',
    '_85__mezzanine_f2_m2_ratio_divergence',
    '_86__mezzanine_test_1',
    '_87__mezzanine_f2_m2_inverse_ratio',
    '_88__mezzanine_f2_m2_inverse_ratio_divergence',
    '_89__mezzanine_test_2',
    '_90__outbuildings_m2',
    '_91__outbuildings_f2',
    '_92__outbuildings_f2_m2_ratio',
    '_93__outbuildings_f2_m2_ratio_divergence',
    '_94__outbuildings_test_1',
    '_95__outbuildings_f2_m2_inverse_ratio',
    '_96__outbuildings_f2_m2_inverse_ratio_divergence',
    '_97__outbuildings_test_2',
    '_98__balcony_m2',
    '_99__balcony_f2',
    '_100__balcony_f2_m2_ratio',
    '_101__balcony_f2_m2_ratio_divergence',
    '_102__balcony_test_1',
    '_103__balcony_f2_m2_inverse_ratio',
    '_104__balcony_f2_m2_inverse_ratio_divergence',
    '_105__balcony_test_2',
    '_106__limited_use_m2',
    '_107__limited_use_f2',
    '_108__limited_use_f2_m2_ratio',
    '_109__limited_use_f2_m2_ratio_divergence',
    '_110__limited_use_test_1',
    '_111__limited_use_f2_m2_inverse_ratio',
    '_112__limited_use_f2_m2_inverse_ratio_divergence',
    '_113__limited_use_test_2',
    '_114__voids_m2',
    '_115__voids_f2',
    '_116__voids_f2_m2_ratio',
    '_117__voids_f2_m2_ratio_divergence',
    '_118__voids_test_1',
    '_119__voids_f2_m2_inverse_ratio',
    '_120__voids_f2_m2_inverse_ratio_divergence',
    '_121__voids_test_2',
    '_122__restricted_height_m2',
    '_123__restricted_height_f2',
    '_124__restricted_height_f2_m2_ratio',
    '_125__restricted_height_f2_m2_ratio_divergence',
    '_126__restricted_height_test_1',
    '_127__restricted_height_f2_m2_inverse_ratio',
    '_128__restricted_height_f2_m2_inverse_ratio_divergence',
    '_129__restricted_height_test_2',
    '_130__conservatory_m2',
    '_131__conservatory_f2',
    '_132__conservatory_f2_m2_ratio',
    '_133__conservatory_f2_m2_ratio_divergence',
    '_134__conservatory_test_1',
    '_135__conservatory_f2_m2_inverse_ratio',
    '_136__conservatory_f2_m2_inverse_ratio_divergence',
    '_137__conservatory_test_2',
    '_138__cottage_m2',
    '_139__cottage_f2',
    '_140__cottage_f2_m2_ratio',
    '_141__cottage_f2_m2_ratio_divergence',
    '_142__cottage_test_1',
    '_143__cottage_f2_m2_inverse_ratio',
    '_144__cottage_f2_m2_inverse_ratio_divergence',
    '_145__cottage_test_2',
    '_146__main_house_m2',
    '_147__main_house_f2',
    '_148__main_house_f2_m2_ratio',
    '_149__main_house_f2_m2_ratio_divergence',
    '_150__main_house_test_1',
    '_151__main_house_f2_m2_inverse_ratio',
    '_152__main_house_f2_m2_inverse_ratio_divergence',
    '_153__main_house_test_2',
    '_154__grand_total_m2',
    '_155__grand_total_f2',
    '_156__grand_total_f2_m2_ratio',
    '_157__grand_total_f2_m2_ratio_divergence',
    '_158__grand_total_test_1',
    '_159__grand_total_f2_m2_inverse_ratio',
    '_160__grand_total_f2_m2_inverse_ratio_divergence',
    '_161__grand_total_test_2',
    '_162__internal_total_m2',
    '_163__internal_total_f2',
    '_164__internal_total_f2_m2_ratio',
    '_165__internal_total_f2_m2_ratio_divergence',
    '_166__internal_total_test_1',
    '_167__internal_total_f2_m2_inverse_ratio',
    '_168__internal_total_f2_m2_inverse_ratio_divergence',
    '_169__internal_total_test_2',
    '_170__external_total_m2',
    '_171__external_total_f2',
    '_172__external_total_f2_m2_ratio',
    '_173__external_total_f2_m2_ratio_divergence',
    '_174__external_total_test_1',
    '_175__external_total_f2_m2_inverse_ratio',
    '_176__external_total_f2_m2_inverse_ratio_divergence',
    '_177__external_total_test_2',
    '_178__description_M2',
    '_179__summary_box_M2',
    '_180__total_M2',
    '_181__grand_total_M2',
    '_182__grand_total_total_M2_M2_ratio',
    '_183__grand_total_total_m2_m2_ratio_divergence',
    '_184__grand_total_total_test',
    '_185__summary_box_total_M2_M2_ratio',
    '_186__summary_box_total_m2_m2_ratio_divergence',
    '_187__summary_box_total_test',
    '_188__summary_box_grand_total_M2_M2_ratio',
    '_189__summary_box_grand_total_m2_m2_ratio_divergence',
    '_190__summary_box_grand_total_test',
    '_191__description_total_M2_M2_ratio',
    '_192__description_total_m2_m2_ratio_divergence',
    '_193__description_total_test',
    '_194__description_grand_total_M2_M2_ratio',
    '_195__description_grand_total_m2_m2_ratio_divergence',
    '_196__description_grand_total_test',
    '_197__description_summary_box_M2_M2_ratio',
    '_198__description_summary_box_m2_m2_ratio_divergence',
    '_199__description_summary_box_test',
    '_200__high_confidence_level_hit'
]


###################### initialise##################################################

for i in columns:
    attach(i)
    print(i)

    
    
##################### export to csv ###############################################    
    
    
out[out['_153__main_house_test_2']!=False][0:3]
out=out.replace(np.nan, ".",regex=True)
out.to_csv('tesseract_ocr_x_xx_validation_pipeline_output.csv',index=False)


######################################################################################


print('number of high conf hits \n = ',sum(out._200__high_confidence_level_hit))





