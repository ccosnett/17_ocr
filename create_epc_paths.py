#!/home/ubuntu/anaconda3/envs/py373/bin/python
path_list = [
    './01_ocr_06_21/',
    './02_ocr_06_28/',
    './03_ocr_07_06/',
    './04_ocr_07_13/',
    './05_ocr_07_20/'
]

image_path_table_path = path_list[0]

# database connection libraries
from sqlalchemy import create_engine
import pymysql

# imports
import pandas as pd
from pandarallel import pandarallel
import regex
import numpy as nb
import pytesseract
import numpy as np
import math
import re
import time
import pyfiglet
import regex
try:
    from PIL import Image
except ImportError:
    import Image

# initialize parallel_apply
pandarallel.initialize(nb_workers=14)
   
# define the mysql-database connection string
connection_str='mysql+mysqldb://{user}:{password}@{server}:{port}/{database}'.format(
    user='admin',
    password='s4erjd9edid9odb', 
    server='avm-big.cokbmcletl0d.eu-west-2.rds.amazonaws.com',
    port = 3306,
    database='ocr')

# create the Object Relational Mapper called sqlEngine
sqlEngine = create_engine(connection_str, echo=True)

# make a connection with the database
dbConnection = sqlEngine.connect()

#pd.read_sql('DROP TABLE IF EXISTS img_files;', dbConnection)


# defining an important function called attach
def attach(func):
    '''this function takes as its argument a function which it uses to create and attach a column to the right of the dataframe called out'''
    out[func] = out.parallel_apply(    eval(func),    axis=1)
    #out[func] = out.progress_apply(    eval(func),    axis=1)
    
# this redundant function returns a list of files for a given `id`
def file_list(row):
    id_=row["id"]
    dataframe=df[df["id"]==id_]
    return list(dataframe["filename"].values)

# this function returns the first name in a file list
def img_file_1(row):
    id_=row['id']
    dataframe=df[df['id']==id_]
    l = list(dataframe["filename"].values)
    if len(l) > 0:
        return l[0]
    else:
        return ''
        
def img_file_2(row):
    id_=row['id']
    dataframe=df[df['id']==id_]
    l = list(dataframe["filename"].values)
    if len(l) > 1:
        return l[1]
    else:
        return ''
    
def img_file_3(row):
    id_=row['id']
    dataframe=df[df['id']==id_]
    l = list(dataframe["filename"].values)
    if len(l) > 2:
        return l[2]
    else:
        return ''
    
def img_file_4(row):
    id_=row['id']
    dataframe=df[df['id']==id_]
    l = list(dataframe["filename"].values)
    if len(l) > 3:
        return l[3]
    else:
        return ''

def img_file_5(row):
    id_=row['id']
    dataframe=df[df['id']==id_]
    l = list(dataframe["filename"].values)
    if len(l) > 4:
        return l[4]
    else:
        return ''


# MAIN PROGRAM BEGINS
for dir_path in path_list:
    images_csv_path = dir_path + '/images.csv'
    print(pyfiglet.figlet_format('creating image_file_paths table'))
    print(images_csv_path)
    # l is the set of id's from images.csv
    df = pd.read_csv(images_csv_path)
    #df = df[df.category =='BUY']
    df = df[(df.category == 'BUY') | (df.category == 'RENT')]
    df = df[df.imgType == 'epc']
    l = list(set(df["id"].values))
    l.sort()

    # adding l to a fresh dataframe called out
    out = pd.DataFrame()
    out["id"] = np.asarray(l)
    

    print('attaching img_file_1 column to frame')
    attach('img_file_1')
    print('attaching img_file_2 column to frame')
    attach('img_file_2')
    print('attaching img_file_3 column to frame')
    attach('img_file_3')
    print('attaching img_file_4 column to frame')
    attach('img_file_4')
    print('attaching img_file_5 column to frame')
    attach('img_file_5')

    out.to_sql('epc_paths', dbConnection, if_exists='append', index=False)

# does it matter if I leave the connection open?
dbConnection.close()
