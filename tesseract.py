#!/home/ubuntu/anaconda3/envs/py373/bin/python
NUM = 9

# database connection libraries
from sqlalchemy import create_engine
import pymysql

# imports
import pandas as pd
from pandarallel import pandarallel
import regex
import numpy as nb
import pytesseract
import numpy as np
import math
import re
import time
import pyfiglet
import regex
try:
    from PIL import Image
except ImportError:
    import Image

# initialize parallel_apply
# pandarallel.initialize(nb_workers=14)
#pandarallel.initialize(nb_workers=NUM, progress_bar = True)
start_time = time.time()

# defining an important function called attach
def attach(func):
    '''this function takes as its argument a function which it uses to create and attach a column to the right of the dataframe called out'''
    out[func] = out.parallel_apply(    eval(func),    axis=1)
    #out[func] = out.progress_apply(    eval(func),    axis=1)
    #out[func] = out.apply(    eval(func),    axis=1)


# define the mysql-database connection string
connection_str='mysql+mysqldb://{user}:{password}@{server}:{port}/{database}'.format(
    user='admin',
    password='s4erjd9edid9odb', 
    server='avm-big.cokbmcletl0d.eu-west-2.rds.amazonaws.com',
    port = 3306,
    database='ocr')

# create the Object Relational Mapper called sqlEngine
sqlEngine = create_engine(connection_str, echo=True)

# make a connection with the database
dbConnection = sqlEngine.connect()

#pd.read_sql('DROP TABLE IF EXISTS img_files;', dbConnection)


######### ocr columns ######################################
#n = 0
def tesseract_ocr_file_1(row):
#    global n
#    n = n + 1
#    print(n)
#    print('./00_images/' + row['img_file_1'][1:])
    try:
        return pytesseract.image_to_string(Image.open('./00_images/' + row['img_file_1'][1:]))   
    except:
        return ''
    
#def tesseract_ocr_file_1(row):
#    path = row["img_file_1"]
#    if path == '.':
#        return path
#    else:
#        return pytesseract.image_to_string(Image.open(path))    
def tesseract_ocr_file_2(row):
    path = './00_images/' + row['img_file_2'][1:]
    if path == './00_images/':
        return ''
    else:
        try:
            return pytesseract.image_to_string(Image.open(path))
        except:
            return ''
    
def tesseract_ocr_file_3(row):
    path = './00_images/' + row['img_file_3'][1:]
    if path == './00_images/':
        return ''
    else:
        return pytesseract.image_to_string(Image.open(path))  
    
def tesseract_ocr_file_4(row):
    path = './00_images/' + row['img_file_4'][1:]
    if path == './00_images/':
        return ''
    else:
        try:
            return pytesseract.image_to_string(Image.open(path))
        except:
            return ''
    
def tesseract_ocr_file_5(row):
    path = './00_images/' + row['img_file_5'][1:]
    if path == './00_images/':
        return ''
    else:
        try:
            return pytesseract.image_to_string(Image.open(path))
        except:
            return ''
#out2 = out[out["id"].isin(l)]

#out2.to_csv("./file_names_7_13_.csv")




def ocr_concatenation(row):
    l = [
        'tesseract_ocr_file_1',
        'tesseract_ocr_file_2',
        'tesseract_ocr_file_3',
        'tesseract_ocr_file_4',
        'tesseract_ocr_file_5'
    ]
    return row[l[0]] + row[l[1]] + row[l[2]] + row[l[3]] + row[l[4]]



#################### LOADING from database ###################################

print('loading data from database')
print("--- %s seconds ---" % (time.time() - start_time))

print(pyfiglet.figlet_format('loading all data from image_fle_paths'))

out = pd.read_sql('SELECT * FROM ocr.image_file_paths;', dbConnection)

print("--- %s seconds ---" % (time.time() - start_time))







#################### DOING OCR ###################################

# performing the ocr

print(pyfiglet.figlet_format('doing OCR'))




pandarallel.initialize(nb_workers=NUM, progress_bar=True)
attach('tesseract_ocr_file_1')
print("--- %s seconds ---" % (time.time() - start_time))
       
print(pyfiglet.figlet_format('turning off progress bars'))
pandarallel.initialize(nb_workers=NUM, progress_bar=False)

attach('tesseract_ocr_file_2')
print("--- %s seconds ---" % (time.time() - start_time))

attach('tesseract_ocr_file_3')
print("--- %s seconds ---" % (time.time() - start_time))

attach('tesseract_ocr_file_4')
print("--- %s seconds ---" % (time.time() - start_time))
attach('tesseract_ocr_file_5')
print("--- %s seconds ---" % (time.time() - start_time))
attach('ocr_concatenation')


#################### OUTPUT FILE  ###################################
out.to_csv('./tesseract_output.csv', index=False)
out.to_sql('tesseract', dbConnection, if_exists = 'replace', index = False)




dbConnection.close()

